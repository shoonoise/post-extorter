#!/usr/bin/env python
# coding=utf-8
import logging

import argparse
from suds import WebFault
from suds.client import Client
from suds.bindings import binding
from suds.plugin import MessagePlugin
from pymongo import MongoClient

binding.envns = ('SOAP-ENV', 'http://www.w3.org/2003/05/soap-envelope')

FINAL_OPERATIONS = [u"Вручение",
                    u"Передача на временное хранение",
                    u"Уничтожение",
                    u"Оформление в собственность",
                    u"Регистрация утраты"]


class MessageFixer(MessagePlugin):
    def marshalled(self, context):
        body = context.envelope.getChild('Body')
        wrong_elem = body[0]
        for elem in wrong_elem:
            elem.prefix = 'ns1'


def get_status_for_track(track_no):
    url = 'https://tracking.russianpost.ru/rtm34?wsdl'
    client = Client(url, headers={'Content-Type': 'application/soap+xml; charset=utf-8'}, plugins=[MessageFixer()])

    operation_history_request = client.factory.create('ns1:OperationHistoryRequest')
    operation_history_request.Barcode = track_no
    operation_history_request.MessageType = 0

    auth_header = client.factory.create('ns1:AuthorizationHeader')
    auth_header.login = 'AMzQXKKNlgQgDC'
    auth_header.password = '0gI4ngDvl4of'

    try:
        r = client.service.getOperationHistory(operation_history_request, auth_header)
    except WebFault, e:
        print "Unable to get track status: %s" % e.fault
    else:
        oper_param = r[0][-1]['OperationParameters']
        item_param = r[0][-1]['ItemParameters']
        status = oper_param['OperType']['Name']
        last_oper = None

        if unicode(status) in FINAL_OPERATIONS:
            last_oper = oper_param['OperDate']

        item_mass = None
        if hasattr(item_param, 'Mass'):
            item_mass = item_param['Mass']

        return {
            "status": status,
            "description": oper_param['OperAttr']['Name'],
            "item_mass": item_mass,
            "first_oper": r[0][0]['OperationParameters']['OperDate'],
            "last_oper": last_oper,
        }


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--mongodb", help="mongo connection url", default="mongodb://localhost:27017")
    parser.add_argument("--dbname", help="mongo database name url", default="successcrm")
    parser.add_argument("--new", help="Check only new tracks", default=False, action="store_true")
    parser.add_argument("--debug", help="Print debug logs", default=False, action="store_true")

    args = parser.parse_args()

    if args.debug:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(format="%(asctime)s %(message)s", level=level)
    logging.getLogger('suds').setLevel(logging.ERROR)

    client = MongoClient(args.mongodb)
    leads = client[args.dbname]["Lead"]

    for lead in leads.find():
        track_number = lead.get("trackNumber")

        if not track_number:
            logging.debug("Skip %s lead because no track number", lead['_id'])
            continue

        if lead.get("trackStatus") and args.new:
            logging.debug("Skip %s lead as flag `new` was passed, but lead has status", lead['_id'])
            continue

        if lead.get("trackStatus") and lead["trackStatus"].get("final"):
            logging.debug("Skip %s lead because item's status (%s) is final", lead['_id'], lead["trackStatus"]["status"])
            continue

        logging.info("Look for status for %s", track_number)
        status = get_status_for_track(track_number)

        if not status:
            continue

        lead["trackStatus"] = {'status': unicode(status["status"]),
                               'description': unicode(status["description"]),
                               'mass': status["item_mass"],
                               'send': status["first_oper"],
                               'final': status["last_oper"]}

        logging.info("Update track info: %s", lead["trackStatus"])

        leads.update_one({"_id": lead["_id"]}, {"$set": lead}, upsert=False)


if __name__ == "__main__":
    main()
